package br.senai.sp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import org.springframework.lang.UsesSunHttpServer;
import org.springframework.stereotype.Repository;

import br.senai.sp.modelo.Usuario;

@Repository
public class UsuarioRepository {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	public void inserir(Usuario usuario) {
		this.manager.persist(usuario);
	}

	public Usuario buscar(String login, String senha) {
		try {
			TypedQuery<Usuario> query = this.manager
					.createQuery("select u from Usuario u where u.login = :login and u.senha = :senha", Usuario.class);
			query.setParameter("login", login);
			query.setParameter("senha", senha);
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Transactional
	public void alterar(Usuario usuario) {
		this.manager.merge(usuario);
	}

	public Usuario buscar(Long id) {
		return this.manager.find(Usuario.class, id);
	}

	@Transactional
	public void excluir(Long id) {
		this.manager.remove(this.manager.find(Usuario.class, id));
	}

	public List<Usuario> listar() {
		TypedQuery<Usuario> query = this.manager.createQuery("select u from Usuario u", Usuario.class);
		return query.getResultList();
	}
}
