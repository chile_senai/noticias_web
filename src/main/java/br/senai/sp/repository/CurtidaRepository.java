package br.senai.sp.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.senai.sp.modelo.Curtida;

@Repository
public class CurtidaRepository {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	public void inserir(Curtida curtida) {
		manager.persist(curtida);
	}

	public Curtida buscarCurtida(Long idNoticia, Long idUsuario) {
		try {
			TypedQuery<Curtida> query = manager.createQuery(
					"select c from Curtida c where c.noticia.id = :idNoticia and c.usuario.id = :idUsuario",
					Curtida.class);
			query.setParameter("idNoticia", idNoticia);
			query.setParameter("idUsuario", idUsuario);
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Transactional
	public void remover(Curtida curtida) {
		manager.remove(manager.getReference(Curtida.class, curtida.getId()));
	}
}
