package br.senai.sp.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.senai.sp.modelo.SecurityToken;
import br.senai.sp.modelo.Usuario;

@Repository
public class SecurityTokenRepository {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	public void inserir(SecurityToken securityToken) {
		this.manager.persist(securityToken);
	}

	@Transactional
	public void atualizar(SecurityToken securityToken) {
		this.manager.merge(securityToken);
	}

	public SecurityToken buscarPorUsuario(Long idUsuario) {
		try {
			TypedQuery<SecurityToken> query = this.manager
					.createQuery("select s from SecurityToken s where s.usuario.id = :id", SecurityToken.class);
			query.setParameter("id", idUsuario);
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public SecurityToken buscarPorToken(String token) {
		try {
			TypedQuery<SecurityToken> query = this.manager
					.createQuery("select s from SecurityToken s where s.token = :token", SecurityToken.class);
			query.setParameter("token", token);
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Transactional
	public void excluirTokenUsuario(Long idUsuario) {
		this.manager.remove(buscarPorUsuario(idUsuario));
	}
}
