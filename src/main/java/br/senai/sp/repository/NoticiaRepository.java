package br.senai.sp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.senai.sp.modelo.Noticia;

@Repository
public class NoticiaRepository {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	public void inserir(Noticia noticia) {
		this.manager.persist(noticia);
	}

	@Transactional
	public void aprovarNoticia(Long idNoticia) {
		Query query = manager.createQuery("update Noticia n set n.aprovada = :aprovacao where n.id = :id");
		query.setParameter("aprovacao", true);
		query.setParameter("id", idNoticia);
		query.executeUpdate();
	}

	public Noticia buscar(Long idNoticia) {
		return manager.find(Noticia.class, idNoticia);
	}

	public List<Noticia> listarTodas() {
		TypedQuery<Noticia> query = manager.createQuery("select n from Noticia n order by n.dataCriacao desc",
				Noticia.class);
		return query.getResultList();
	}

	public List<Noticia> listar() {
		TypedQuery<Noticia> query = manager.createQuery(
				"select n from Noticia n where n.aprovada = true order by n.dataCriacao desc", Noticia.class);
		return query.getResultList();
	}

	@Transactional
	public void excluir(Long idNoticia) {
		manager.remove(buscar(idNoticia));
	}

}
