package br.senai.sp.interceptor;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.senai.sp.anotacao.Privado;
import br.senai.sp.anotacao.Publico;
import br.senai.sp.modelo.ErroHttp;
import br.senai.sp.modelo.SecurityToken;
import br.senai.sp.modelo.Usuario;
import br.senai.sp.repository.SecurityTokenRepository;
import br.senai.sp.util.SecurityUtil;

public class SecurityInterceptor extends HandlerInterceptorAdapter {
	@Autowired
	private SecurityTokenRepository repository;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		// if (true)
		// return true;
		// verifica se o handler � uma inst�ncia de HandlerMethod, o que indica
		// que foi encontrado o m�todo associado � requisi��o
		if (handler instanceof HandlerMethod) {
			HandlerMethod metodo = (HandlerMethod) handler;
			// caso seja p�blico libera
			if (metodo.getMethodAnnotation(Publico.class) != null) {
				return true;
			}
			// verifica se o acesso � privado
			Privado privadoAnnot = metodo.getMethodAnnotation(Privado.class);
			if (privadoAnnot != null) {
				// extrai o token da requisi��o
				String token = request.getHeader("Authorization");
				// verifica se o token � v�lido, caso n�o seja retorna o erros
				try {
					SecurityUtil.validaToken(token);
				} catch (Exception e) {
					return acessoNegado(e.getMessage(), response);
				}
				SecurityToken securityToken = repository.buscarPorToken(token);
				if (securityToken == null) {
					return acessoNegado("Token n�o encontrado", response);
				}
				Usuario usuario = securityToken.getUsuario();
				if (usuario == null) {
					return acessoNegado("Usu�rio nulo", response);
				}
				System.out.println(usuario.getTipoUsuario());
				if (Arrays.asList
						(privadoAnnot.tipoUsuario()).contains(usuario.getTipoUsuario())) {
					request.setAttribute("usuarioLogado", usuario);
					return true;
				}
			}
		}
		return acessoNegado("Acesso Negado", response);
	}

	private boolean acessoNegado(String descricao, HttpServletResponse response) throws IOException {
		response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		response.getWriter()
				.println(new ObjectMapper().writeValueAsString((new ErroHttp(HttpStatus.FORBIDDEN, descricao))));
		response.setStatus(HttpStatus.FORBIDDEN.value());
		return false;
	}
}
