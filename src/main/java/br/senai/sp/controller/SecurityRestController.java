package br.senai.sp.controller;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.anotacao.Publico;
import br.senai.sp.modelo.ErroHttp;
import br.senai.sp.modelo.SecurityToken;
import br.senai.sp.modelo.Usuario;
import br.senai.sp.repository.SecurityTokenRepository;
import br.senai.sp.repository.UsuarioRepository;
import br.senai.sp.util.SecurityUtil;

@RestController
@RequestMapping("/logar")
public class SecurityRestController {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private SecurityTokenRepository securityRepository;

	@Publico
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> logar(@RequestBody Usuario usuario) {
		try {
			usuario = usuarioRepository.buscar(usuario.getLogin(), usuario.getSenha());
			if (usuario == null) {
				throw new Exception("Usu�rio ou senha inv�lidos");
			}
			// calcula a data de expira��o como meia noite do dia atual
			Calendar proximaExpiracao = Calendar.getInstance();
			proximaExpiracao.set(Calendar.HOUR_OF_DAY, 23);
			proximaExpiracao.set(Calendar.MINUTE, 59);
			proximaExpiracao.set(Calendar.SECOND, 59);
			String token = SecurityUtil.gerarTokenPorUsuario(usuario, proximaExpiracao);
			// busca o securityToken pelo usuario
			SecurityToken securityToken = securityRepository.buscarPorUsuario(usuario.getId());
			// verifica se j� existe token gerado para esse usu�rio
			if (securityToken == null) {				
				securityToken = new SecurityToken(token, proximaExpiracao, usuario);
				securityRepository.inserir(securityToken);
			} else {
				securityToken.atualizarToken(token, proximaExpiracao);
				securityRepository.atualizar(securityToken);
			}
			// retorna o token gerado
			return ResponseEntity.ok(securityToken);
		} catch (Exception e) {
			e.printStackTrace();
			ErroHttp erro = new ErroHttp(HttpStatus.UNAUTHORIZED, e.getMessage());
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(erro);
		}
	}
}
