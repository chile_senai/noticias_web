package br.senai.sp.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.anotacao.Privado;
import br.senai.sp.anotacao.Publico;
import br.senai.sp.modelo.ErroHttp;
import br.senai.sp.modelo.TipoUsuario;
import br.senai.sp.modelo.Usuario;
import br.senai.sp.repository.SecurityTokenRepository;
import br.senai.sp.repository.UsuarioRepository;

@RestController
@RequestMapping("/usuario")
public class UsuarioRestController {
	@Autowired
	private UsuarioRepository repository;

	@Autowired
	private SecurityTokenRepository repositoryToken;

	@Publico
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> novoUsuario(@RequestBody Usuario usuario) {
		try {
			repository.inserir(usuario);
			return ResponseEntity.created(URI.create("/usuario/" + usuario.getId())).body(usuario);
		} catch (Exception e) {
			ErroHttp erro = new ErroHttp(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			return ResponseEntity.status(erro.getStatusCode()).body(erro);
		}
	}

	@Privado(tipoUsuario = { TipoUsuario.MODERADOR })
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> listar() {
		try {
			return new ResponseEntity<Object>(repository.listar(), HttpStatus.OK);
		} catch (Exception e) {
			ErroHttp erro = new ErroHttp(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			return ResponseEntity.status(erro.getStatusCode()).body(erro);
		}
	}

	@Privado(tipoUsuario = { TipoUsuario.MODERADOR, TipoUsuario.USUARIO_COMUM })
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> alterarUsuario(@PathVariable("id") Long id, @RequestBody Usuario usuario) {
		try {
			usuario.setId(id);
			repository.alterar(usuario);
			HttpHeaders header = new HttpHeaders();
			header.setLocation(URI.create("/usuario/" + usuario.getId()));
			return new ResponseEntity<>(usuario, header, HttpStatus.OK);
		} catch (Exception e) {
			ErroHttp erro = new ErroHttp(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			return ResponseEntity.status(erro.getStatusCode()).body(erro);
		}
	}

	@Privado(tipoUsuario = { TipoUsuario.MODERADOR, TipoUsuario.USUARIO_COMUM })
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> buscarUsuario(@PathVariable("id") Long id) {
		try {
			return new ResponseEntity<Object>(repository.buscar(id), HttpStatus.OK);
		} catch (Exception e) {
			ErroHttp erro = new ErroHttp(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			return ResponseEntity.status(erro.getStatusCode()).body(erro);
		}
	}

	@Privado(tipoUsuario = { TipoUsuario.MODERADOR, TipoUsuario.USUARIO_COMUM })
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deletarUsuario(@PathVariable("id") Long id) {
		try {
			repositoryToken.excluirTokenUsuario(id);
			repository.excluir(id);
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			ErroHttp erro = new ErroHttp(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			return ResponseEntity.status(erro.getStatusCode()).body(erro);
		}
	}

}
