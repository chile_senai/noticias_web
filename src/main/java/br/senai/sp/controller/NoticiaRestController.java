package br.senai.sp.controller;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.anotacao.Privado;
import br.senai.sp.modelo.Curtida;
import br.senai.sp.modelo.ErroHttp;
import br.senai.sp.modelo.Noticia;
import br.senai.sp.modelo.TipoUsuario;
import br.senai.sp.modelo.Usuario;
import br.senai.sp.repository.CurtidaRepository;
import br.senai.sp.repository.NoticiaRepository;

@RestController
@RequestMapping("/noticia")
public class NoticiaRestController {
	@Autowired
	private NoticiaRepository repository;

	@Autowired
	private CurtidaRepository curtidaRepository;

	@Privado(tipoUsuario = { TipoUsuario.MODERADOR, TipoUsuario.USUARIO_COMUM })
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> novaNoticia(@RequestBody Noticia noticia, HttpServletRequest request) {
		try {
			Usuario usuarioLogado = (Usuario) request.getAttribute("usuarioLogado");
			noticia.setUsuario(usuarioLogado);
			repository.inserir(noticia);
			return ResponseEntity.created(URI.create("/noticia/" + noticia.getId())).body(noticia);
		} catch (Exception e) {
			ErroHttp erro = new ErroHttp(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			return ResponseEntity.status(erro.getStatusCode()).body(erro);
		}
	}

	@Privado(tipoUsuario = { TipoUsuario.MODERADOR })
	@RequestMapping(value = "/{idNoticia}/aprovar", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> aprovarNoticia(@PathVariable Long idNoticia) {
		try {
			repository.aprovarNoticia(idNoticia);
			HttpHeaders header = new HttpHeaders();
			header.setLocation(URI.create("/noticia/" + idNoticia));
			return new ResponseEntity<>(repository.buscar(idNoticia), header, HttpStatus.OK);
		} catch (Exception e) {
			ErroHttp erro = new ErroHttp(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			return ResponseEntity.status(erro.getStatusCode()).body(erro);
		}
	}

	@Privado(tipoUsuario = { TipoUsuario.USUARIO_COMUM, TipoUsuario.MODERADOR })
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> listarNoticias(HttpServletRequest request) {
		try {
			Usuario usuarioLogado = (Usuario) request.getAttribute("usuarioLogado");
			List<Noticia> noticias;
			if (usuarioLogado.getTipoUsuario() == TipoUsuario.MODERADOR) {
				noticias = repository.listarTodas();
			} else {
				noticias = repository.listar();
			}
			noticias.forEach(noticia -> {
				noticia.getCurtidas().forEach(curtida -> {
					if (curtida.getUsuario().getId() == usuarioLogado.getId()) {
						noticia.setUsuarioCurtiu(true);
					}
				});
			});
			return new ResponseEntity<Object>(noticias, HttpStatus.OK);
		} catch (Exception e) {
			ErroHttp erro = new ErroHttp(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			return ResponseEntity.status(erro.getStatusCode()).body(erro);
		}
	}
	
	
	

	@Privado(tipoUsuario = { TipoUsuario.USUARIO_COMUM, TipoUsuario.MODERADOR })
	@RequestMapping(value = "/{id}/curtir", method = {RequestMethod.POST, RequestMethod.DELETE})
	public ResponseEntity<Object> curtirNoticia(@PathVariable("id") Long idNoticia, HttpServletRequest request) {
		try {
			Usuario usuarioLogado = (Usuario) request.getAttribute("usuarioLogado");
			Curtida curtida = curtidaRepository.buscarCurtida(idNoticia, usuarioLogado.getId());			
			if (curtida == null) {
				curtida = new Curtida();
				Noticia noticia = repository.buscar(idNoticia);
				curtida.setNoticia(noticia);
				curtida.setUsuario(usuarioLogado);
				noticia.getCurtidas().add(curtida);
				curtidaRepository.inserir(curtida);
				return new ResponseEntity<Object>(curtida, HttpStatus.OK);
			}else {
				curtidaRepository.remover(curtida);
				return ResponseEntity.noContent().build();	
			}			
		} catch (Exception e) {
			e.printStackTrace();
			ErroHttp erro = new ErroHttp(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			return ResponseEntity.status(erro.getStatusCode()).body(erro);
		}
	}

	@Privado(tipoUsuario = { TipoUsuario.USUARIO_COMUM, TipoUsuario.MODERADOR })
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> excluirNoticia(@PathVariable("id") Long idNoticia) {
		try {
			repository.excluir(idNoticia);
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			ErroHttp erro = new ErroHttp(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			return ResponseEntity.status(erro.getStatusCode()).body(erro);
		}
	}
}
