package br.senai.sp.modelo;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties("curtidas")
public class Noticia {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String titulo;
	private String texto;
	@ManyToOne
	private Usuario usuario;
	private boolean aprovada;
	@OneToMany
		(mappedBy = "noticia", 
			fetch=FetchType.EAGER, 
				orphanRemoval=true)
	private List<Curtida> curtidas;
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat
		(pattern = "dd/MM/yyyy HH:mm:ss zzz", 
			timezone = "Brazil/East")
	private Calendar dataCriacao = Calendar.getInstance();
	@Transient
	private boolean usuarioCurtiu;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isAprovada() {
		return aprovada;
	}

	public void setAprovada(boolean aprovada) {
		this.aprovada = aprovada;
	}

	public List<Curtida> getCurtidas() {
		return curtidas;
	}

	public void setCurtidas(List<Curtida> curtidas) {
		this.curtidas = curtidas;
	}

	public Calendar getDataCriacao() {
		return dataCriacao;
	}
	
	public int getQtdCurtidas(){
		if(curtidas != null){
			return curtidas.size();
		}
		return 0;
	}

	public boolean isUsuarioCurtiu() {
		return usuarioCurtiu;
	}

	public void setUsuarioCurtiu(boolean usuarioCurtiu) {
		this.usuarioCurtiu = usuarioCurtiu;
	}
			

}
