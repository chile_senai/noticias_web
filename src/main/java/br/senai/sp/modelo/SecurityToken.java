package br.senai.sp.modelo;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class SecurityToken implements Serializable{	
	private String token;
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Brazil/East")
	private Calendar expiracao;
	@Id
	@OneToOne		
	private Usuario usuario;

	public SecurityToken() {
	}

	public SecurityToken(String token, Calendar expiracao, Usuario usuario) {
		this.token = token;
		this.expiracao = expiracao;
		this.usuario = usuario;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Calendar getExpiracao() {
		return expiracao;
	}

	public void setExpiracao(Calendar expiracao) {
		this.expiracao = expiracao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void atualizarToken(String token, Calendar expiracao) {
		this.token = token;
		this.expiracao = expiracao;
	}

	public void expirarToken() {
		this.atualizarToken("", Calendar.getInstance());
	}

}
