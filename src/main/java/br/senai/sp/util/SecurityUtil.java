package br.senai.sp.util;

import java.util.Calendar;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;

import br.senai.sp.modelo.SecurityToken;
import br.senai.sp.modelo.Usuario;

public class SecurityUtil {
	private static final String TOKEN_SECRET = "31bc2cb67d5e44a40a4fd780fbcab20b";

	public static String gerarTokenPorUsuario(final Usuario usuario, final Calendar expiracao) throws Exception {
		// gera o token
		Algorithm algoritmo = Algorithm.HMAC256(TOKEN_SECRET);
		String token = 
				JWT.create().
				withClaim("userId", usuario.getId()).withExpiresAt(expiracao.getTime())
				.sign(algoritmo);
		return token;
	}

	public static void validaToken(String token) throws Exception {
		try {
			Algorithm algoritmo = Algorithm.HMAC256(TOKEN_SECRET);
			JWTVerifier verifier = JWT.require(algoritmo).build();
			verifier.verify(token);
		} catch (TokenExpiredException e) {
			throw new Exception("Token expirado");
		} catch (SignatureVerificationException e) {
			throw new Exception("Assinatura do token inv�lida");
		} catch (Exception e) {
			throw new Exception("Token inv�lido");
		}
	}

}
